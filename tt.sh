#!/bin/bash --
# Sat Jun 23 03:36:24 EDT 2018
# haqler

usage_print()
{
	# here document. Write text to output until stop word, in this case it's 'END'
	cat << "END"
Usage:
	show_args.sh --conv_h your_height_for_convert  --conv_w your_width_for_convert  --img_cnt count_of_images

Parameters:
	--conv_h   -   height parameter for convert tool. Mandatory.
	--conv_w   -   width parameter for convert tool. Mandatory.
	--img_cnt  -   count of images. Temporary paramter, just for testing.

END
}

if [ "$#" -ne 6 ]; then
	usage_print
	exit 1
fi

# process user parameters
while [ "$#" -ne 0 ]; do
	case "$1" in
		"--conv_h")
			CONV_HEIGHT="$2"
			shift 2
			;;

		"--conv_w")
			CONV_WIDTH="$2"
			shift 2
			;;

		"--img_cnt")
			IMAGE_CNT="$2"
			shift 2
			;;
		*)
			usage_print
			exit 1
			;;
	esac

done

#rectangle_area=$(echo "scale=2; $CONV_WIDTH * $CONV_HEIGHT / $IMAGE_CNT" | bc)   # calculate area of one square
#RECT_SIDE=$(echo "sqrt($rectangle_area)" | bc | awk '{printf "%d", $1; exit}')   # find length of square sides
#COLUMN_NUM=$(echo "scale=1; $CONV_WIDTH / $RECT_SIDE" | bc)
#ROW_NUM=$(echo "scale=1; ($CONV_HEIGHT / $RECT_SIDE)" | bc)
#echo 'COLUMN_NUM before='$COLUMN_NUM
#echo 'ROW_NUM before='$ROW_NUM

#COLUMN_NUM=$(echo $COLUMN_NUM | awk ' /\.[5-9]/ {printf "%d", $1 + 1; exit}; {printf "%d", $1}')
#ROW_NUM=$(echo $ROW_NUM | awk ' /\.[1-9]/ {printf "%d", $1 + 1; exit} {printf "%d", $1}')

ROW_NUM=1
RECT_SIDE=$CONV_HEIGHT

while
	NESTED_CNT=$(echo "$CONV_WIDTH / $RECT_SIDE) * $ROW_NUM" | bc)
	[ $IMAGE_CNT -gt $NESTED_CNT ]
do
	ROW_NUM=$((ROW_NUM * 2))
	RECT_SIDE=$((CONV_HEIGHT / 2))	
done
COLUMN_NUM=$(echo "$CONV_WIDTH / $RECT_SIDE)" | bc)



echo 'Width: '$CONV_WIDTH
echo 'Height: '$CONV_HEIGHT
echo 'Picture size: '$RECT_SIDE
echo 'Column number: '$COLUMN_NUM
echo 'Row number: '$ROW_NUM
echo 
echo "metapixel-prepare -r ./pictures/ ./resized --width=$RECT_SIDE --height=$RECT_SIDE"
echo "montage resized/*.png -tile ${ROW_NUM}x${COLUMN_NUM} -mode Concatenate groupphoto.jpg"
echo "convert -resize ${CONV_WIDTH}x${CONV_HEIGHT} groupphoto.jpg cover.jpg"


exit 0

