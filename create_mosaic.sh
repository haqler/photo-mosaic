#!/bin/bash --
# Sat Jun 23 03:36:24 EDT 2018
# haqler

usage_print()
{
	# here document. Write text to output until stop word, in this case it's 'END'
	cat << "END"
Usage:
	show_args.sh --conv_h your_height_for_convert  --conv_w your_width_for_convert  --img_cnt count_of_images

Parameters:
	--conv_h   -   height parameter for convert tool. Mandatory.
	--conv_w   -   width parameter for convert tool. Mandatory.
	--img_cnt  -   count of images. Temporary paramter, just for testing.

END
}

min()
{
	L1=$(echo "$CONV_HEIGHT / $1" | bc)
	L2=$(echo "$CONV_WIDTH / $2" | bc)

	if [ "$L1" -lt "$L2" ]; then
		L=$L1
	else
		L=$L2
	fi
}

if [ "$#" -ne 6 ]; then
	usage_print
	exit 1
fi

# process user parameters
while [ "$#" -ne 0 ]; do
	case "$1" in
		"--conv_h")
			CONV_HEIGHT="$2"
			shift 2
			;;

		"--conv_w")
			CONV_WIDTH="$2"
			shift 2
			;;

		"--img_cnt")
			IMAGE_CNT="$2"
			shift 2
			;;
		*)
			usage_print
			exit 1
			;;
	esac

done

until_num=$(echo "sqrt($IMAGE_CNT.00)" | bc)
#echo 'until='$until_num
for i in `seq $until_num`
do
	nRows=$i
	nCols=$(echo "scale=2; $IMAGE_CNT / $nRows" | bc | awk ' /\.[1-9]/ {printf "%d", $1 + 1; exit} {printf "%d", $1}') # add 1 if result is float number

	L=''
	min $nRows $nCols

	#echo 'nRows='$nRows
	#echo 'nCols='$nCols
	#echo 'L='$L
	#echo '---------'
done

echo 'Width: '$CONV_WIDTH
echo 'Height: '$CONV_HEIGHT
echo 'Picture size: '$L
echo 'Column number: '$nCols
echo 'Row number: '$nRows
echo 
echo "metapixel-prepare -r ./pictures/ ./resized --width=$L --height=$L"
echo "montage resized/*.png -tile ${nRows}x${nCols} -mode Concatenate groupphoto.jpg"
echo "convert -resize ${CONV_WIDTH}x${CONV_HEIGHT} groupphoto.jpg cover.jpg"


exit 0

